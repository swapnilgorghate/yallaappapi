﻿using FallaApp.Model;
using System.Collections.Generic;


namespace FallaApp.Business
{
  public interface IUserRegistrationManager : IManagerBase
  {
    IEnumerable<User> GetAll();
    User Create(User user);
    int UpdateUser(User user);
    int UpdateUserPeers(User user);
    IEnumerable<Country> GetAllCountries();
    IEnumerable<Hobby> GetAllHobbies();
    IEnumerable<Campus> GetAllCampuses();
    IEnumerable<Terms> TermsAndConditions();
    User ValidateOTP(User user);
  }
}
