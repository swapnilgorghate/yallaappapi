﻿using FallaApp.Model;

namespace FallaApp.Business
{
  public interface IAuthenticationManager: IManagerBase
  {
    Authentication Get(int authId);
  }
}
