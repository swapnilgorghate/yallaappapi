﻿using FallaApp.Data;
using FallaApp.Model;
using FallApp.Business.Impl;

namespace FallaApp.Business.Impl
{
  public class AuthenticationManager: ManagerBase, IAuthenticationManager
  {
    private readonly IAuthenticationRepository _authenticationRepository;

    public AuthenticationManager(IAuthenticationRepository authenticationRepository)
    {
      _authenticationRepository = authenticationRepository;
    }
    public Authentication Get(int authId)
    {
      return _authenticationRepository.Get(authId);
    }
  }
}
