﻿using FallaApp.Business;
using System.Linq;
using System.Reflection;
using System.Runtime.Caching;


namespace FallApp.Business.Impl
{
  /// <summary>
  /// This is a base class for all the controllers
  /// </summary>
  public abstract class ManagerBase : IManagerBase
  {
    public void SetContext(IRequestContext context)
    {
      Context = context;
      var managers =
          GetType()
              .GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
              .Where(f => typeof(IManagerBase).IsAssignableFrom(f.FieldType));
      foreach (var manager in managers)
      {
        ((ManagerBase)manager.GetValue(this)).Context = context;
      }
    }

    /// <summary>
    /// Stores the context for the request being processed (User ID and Company User ID).
    /// </summary>
    protected IRequestContext Context { get; set; }

    public void ClearCache()
    {
      var cacheKeys = MemoryCache.Default.Select(kvp => kvp.Key).ToList();
      foreach (var cacheKey in cacheKeys)
      {
        MemoryCache.Default.Remove(cacheKey);
      }
    }

  }
}