﻿using FallaApp.Data;
using FallApp.Business.Impl;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;

namespace FallaApp.Business.Impl
{
  public class MediaManager : ManagerBase, IMediaManager
  {
    private readonly IMediaRepository _mediaRepository;

    public MediaManager(IMediaRepository mediaRepository)
    {
      _mediaRepository = mediaRepository;
    }

    private static string UploadBlob(string fileName, Stream fileStream, string mediaType, string AzureContainer = "AzureContainer")
    {
      var azureContainer = ConfigurationManager.AppSettings[AzureContainer];

      //if (id <= 0)
      //{
      //  throw new ArgumentNullException("Invalid user id");
      //}

      if (string.IsNullOrEmpty(fileName))
      {
        throw new ArgumentNullException("filename invalid");
      }

      try
      {
        var storageAccount =
            CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"]);

        var blobClient = storageAccount.CreateCloudBlobClient();

        // Retrieve a reference to a container.
        var container = blobClient.GetContainerReference(azureContainer);
        container.CreateIfNotExists();

        container.SetPermissions(
            new BlobContainerPermissions
            {
              PublicAccess =
                    BlobContainerPublicAccessType.Blob
            });

        // Create the container if it doesn't already exist.
        container.CreateIfNotExists();

        fileName = string.IsNullOrEmpty(mediaType)
            ? (fileName)
            : (mediaType + "/" + fileName);

        // Retrieve reference to a blob named "myblob".
        var blockBlob = container.GetBlockBlobReference(fileName);
        // Create or overwrite the "myblob" blob with contents from a local file.
        blockBlob.UploadFromStream(fileStream);
      }
      catch (Exception ex)
      {
        throw ex;
      }

      // this will be stored in the database
      return azureContainer + "/" + fileName;
    }
    public string CreateProfileBlob(string fileName, Stream fileStream)
    {
      var fileUri = UploadBlob(fileName, fileStream, "");

      if (!_mediaRepository.UpdateUserProfileImage(Context.UserId, fileUri))
      {
       // throw new Exception("");
      }

      return fileUri;
    }
  }
}
