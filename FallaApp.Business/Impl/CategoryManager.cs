﻿using FallaApp.Data;
using FallaApp.Model;
using FallApp.Business.Impl;
using System.Collections.Generic;


namespace FallaApp.Business.Impl
{
  public class CategoryManager : ManagerBase, ICategoryManager
  {
    private readonly ICategoryRepository _categoryRepository;

    public CategoryManager(ICategoryRepository categoryRepository)
    {
      _categoryRepository = categoryRepository;
    }
    public IEnumerable<Category> GetAll()
    {
      return _categoryRepository.GetAll();
    }
  }
}
