﻿using System;
using System.Collections.Generic;
using FallaApp.Model;
using FallApp.Business.Impl;
using FallaApp.Data;


namespace FallaApp.Business.Impl
{
  public class UserRegistrationManager: ManagerBase, IUserRegistrationManager
  {
    private readonly IUserRegistrationRepository _userRegistrationRepository;

    public UserRegistrationManager(IUserRegistrationRepository userRegistrationRepository)
    {
      _userRegistrationRepository = userRegistrationRepository;
    }

    public IEnumerable<User> GetAll()
    {
      return _userRegistrationRepository.GetAll();
    }
    public User Create(User user)
    {
      var userdata = new User();
      var users = new User();
      var authuserdata = _userRegistrationRepository.Get(new { DeviceId = user.DeviceId, IsActive = true });

      if (authuserdata != null)
      {
        userdata = _userRegistrationRepository.GetUser(new { Id = authuserdata.UserId });
        if (userdata.MSISDN == user.MSISDN)
        {
          userdata.AuthenticationToken = authuserdata.AuthenticationToken;
          return userdata;
        }
        else
        {
          authuserdata.IsActive = false;
          _userRegistrationRepository.Update(authuserdata);
          users = _userRegistrationRepository.Create(user);
        }
      }
      else
      {
        userdata = _userRegistrationRepository.GetUser(new { MSISDN = user.MSISDN });
        if (userdata != null)
        {
          user.Id = userdata.Id;
        }
        users = _userRegistrationRepository.Create(user);
      }
      return users;
    }
    
    public int UpdateUser(User user)
    {
      if (user.FirstName == null)
      {
        throw new ArgumentNullException("FirstName shouldn't be null");
      }
      else if (user.LastName == null)
      {
        throw new ArgumentNullException("FirstName shouldn't be null");
      }
      var userdata = new User();
      var authuserdata = _userRegistrationRepository.Get(new { AuthenticationToken = user.AuthenticationToken, IsActive = true });
      if (authuserdata != null)
      {
        userdata = _userRegistrationRepository.GetUser(new { Id = authuserdata.UserId });
        userdata.FirstName = user.FirstName;
        userdata.LastName = user.LastName;
        userdata.CampusId = userdata.CampusId != null ? userdata.CampusId : user.CampusId;
        userdata.UpdatedOn = DateTime.Now;
      }
      return _userRegistrationRepository.Update(userdata);
    }

    public int UpdateUserPeers(User user)
    {
      if (user.NationalityId == null)
      {
        throw new ArgumentNullException("NationalityId shouldn't be null");
      }
      else if (user.HobbyId == null)
      {
        throw new ArgumentNullException("HobbyId shouldn't be null");
      }
      else if (user.Gender == null)
      {
        throw new ArgumentNullException("Gender shouldn't be null");
      }
      var userdata = new User();
      var authuserdata = _userRegistrationRepository.Get(new { AuthenticationToken = user.AuthenticationToken, IsActive = true });
      if (authuserdata != null)
      {
        userdata = _userRegistrationRepository.GetUser(new { Id = authuserdata.UserId });
        userdata.CreatedOn = userdata.CreatedOn;
        userdata.UpdatedOn = DateTime.Now;
        userdata.NationalityId = userdata.NationalityId != null ? userdata.NationalityId : user.NationalityId;
        userdata.HobbyId = userdata.HobbyId != null ? userdata.HobbyId : user.HobbyId;
        userdata.Gender = user.Gender;
      }
      return _userRegistrationRepository.Update(userdata);
    }

    public IEnumerable<Country> GetAllCountries()
    {
      return _userRegistrationRepository.GetAllCountries();
    }

    public IEnumerable<Hobby> GetAllHobbies()
    {
      return _userRegistrationRepository.GetAllHobbies();
    }

    public IEnumerable<Campus> GetAllCampuses()
    {
      return _userRegistrationRepository.GetAllCampuses();
    }

    public IEnumerable<Terms> TermsAndConditions()
    {
      return _userRegistrationRepository.TermsAndConditions();
    }

    public User ValidateOTP(User user)
    {
      var userdata=new User();
      if (user != null && user.OTP != null)
      {
        if (user.OTP == "12345")
        {
          var authuserdata = _userRegistrationRepository.Get(new { AuthenticationToken = user.AuthenticationToken, IsActive = true });
          userdata = _userRegistrationRepository.GetUser(new { Id = authuserdata.UserId });
        }
        else
        {
          throw new Exception("OTP is not valid.");
        }
      }
      else {
        throw  new ArgumentNullException("OTP is not valid.");
      }
      return userdata;
    }
  }
}
