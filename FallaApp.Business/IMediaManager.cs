﻿using System.IO;

namespace FallaApp.Business
{
  public interface IMediaManager : IManagerBase
  {
    string CreateProfileBlob(string fileName, Stream fileStream);
  }
}
