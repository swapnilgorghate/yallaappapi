﻿using FallaApp.Model;
using System.Collections.Generic;


namespace FallaApp.Business
{
  public interface ICategoryManager: IManagerBase
  {
    IEnumerable<Category> GetAll();
  }
}
