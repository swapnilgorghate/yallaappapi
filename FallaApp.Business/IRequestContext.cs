﻿using System;

namespace FallaApp.Business
{
  public interface IRequestContext
  {
    long UserId { get; set; }
    Guid AuthenticationToken { get; set; }
  }
}
