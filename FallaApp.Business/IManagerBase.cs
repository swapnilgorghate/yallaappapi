﻿
namespace FallaApp.Business
{
  public interface IManagerBase
  {
    void SetContext(IRequestContext context);

    /// <summary>
    /// Clears the Cache by iterating through the keys available in MemoryCache.
    /// </summary>
    void ClearCache();
  }
}
