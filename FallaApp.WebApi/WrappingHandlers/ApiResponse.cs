﻿using FallaApp.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;

namespace FallaApp.WebApi.WrappingHandlers
{
  [DataContract]
  public class ApiResponse
  {
    [DataMember]
    public int StatusCode { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string Message { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public object Result { get; set; }

    public ApiResponse(HttpStatusCode statusCode, object result = null, string ResponseMessage = null)
    {
      var resultcount = 0;

      if(result == null)
      {
        StatusCode = 0;
        if (ResponseMessage == null)
        {
          ResponseMessage = "No Data Found";
        }
      }
      else if (result.GetType().IsGenericType && result is IEnumerable<dynamic>)
      {
        resultcount = (result as IEnumerable<dynamic>).Count();
        if (resultcount == 0)
        {
          StatusCode = 0;
          if (ResponseMessage == null)
          {
            ResponseMessage = "No Data Found";
          }
        }
        else
        {
          StatusCode = 1;
          if (ResponseMessage == null)
          {
            ResponseMessage = "Success";
          }
        }
      }
      else if (result != null)
      {
        StatusCode = 1;
        if (ResponseMessage == null)
        {
          ResponseMessage = "Success";
        }
      }
      
      
      Result = result;
      Message = ResponseMessage;
    }
  }
}

