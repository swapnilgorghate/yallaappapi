﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace FallaApp.WebApi.WrappingHandlers
{
  public class WrappingHandler : DelegatingHandler
  {
    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
    {
      var response = await base.SendAsync(request, cancellationToken);

      return BuildApiResponse(request, response);
    }

    private static HttpResponseMessage BuildApiResponse(HttpRequestMessage request, HttpResponseMessage response)
    {
      object content;
      string message = null;

      if (response.TryGetContentValue(out content) && !response.IsSuccessStatusCode)
      {
        HttpError error = content as HttpError;

        if (error != null)
        {
          content = null;
          message = error.Message;

          message = string.Concat(message, error.ExceptionMessage, error.StackTrace);

        }
      }

      var newResponse = request.CreateResponse(response.StatusCode, new ApiResponse(response.StatusCode, content, message));

      foreach (var header in response.Headers)
      {
        newResponse.Headers.Add(header.Key, header.Value);
      }

      return newResponse;
    }
  }
}