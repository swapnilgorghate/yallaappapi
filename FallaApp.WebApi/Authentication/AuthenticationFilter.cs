﻿using FallaApp.Business; 
using Fallapp.WebApi.Controllers.Base;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;


namespace FallaApp.WebApi.Authentication
{
  public class FAAuthorizeAttribute : AuthorizeAttribute
  {
    private const string AuthorizationHeader = "Authorization";

    protected override bool IsAuthorized(HttpActionContext actionContext)
    {
      AuthorizationResult authorizationResult = null;

      // Get Authorization provider
      var provider =
          (ITokenAuthorizationProvider)
              actionContext.ControllerContext.Configuration.DependencyResolver.GetService(
                  typeof(ITokenAuthorizationProvider));
      // Get Authorization header data
      IEnumerable<string> authorizationHeaders;
      actionContext.Request.Headers.TryGetValues(AuthorizationHeader, out authorizationHeaders);

      // Check Token provided is valid
      if (authorizationHeaders != null)
      {
        var token = authorizationHeaders.FirstOrDefault();

        if (token != null)
        {
          authorizationResult = provider.Authenticate(token);
          HttpContext.Current.Items["AuthorizationResult"] = authorizationResult;
        }
      }

      var isValid = authorizationResult != null && authorizationResult.Success;

      if (!isValid)
      {
        return false;
      }
      // Set the request context
      var context =
          (IRequestContext)
              actionContext.ControllerContext.Configuration.DependencyResolver.GetService(
                  typeof(IRequestContext));
      //context.TenantId = authorizationResult.TenantId;
      context.UserId = authorizationResult.UserId;

      ((ApiControllerBase)actionContext.ControllerContext.Controller).SetMangerContext(context);

      return true;
    }


  }
}
