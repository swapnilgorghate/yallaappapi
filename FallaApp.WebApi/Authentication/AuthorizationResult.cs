﻿using System;

namespace FallaApp.WebApi.Authentication
{
  public class AuthorizationResult
  {
    public bool Success { get; set; }

    #region Success

    public long UserId { get; set; }


    #endregion

    //#region Fail

    //public bool IsAdmin { get; set; }

    //public DateTime? SubscriptionExpiryDate { get; set; }

    //public bool IsSubscriptionExpired { get; set; }

    //public Exception Exception { get; set; }

    //#endregion
  }
}