﻿namespace FallaApp.WebApi.Authentication
{
  public interface ITokenAuthorizationProvider
  {
    /// <summary>
    /// Authenticates the temporary token received with the server value and sets the Authentication result
    /// </summary>
    /// <param name="token"></param>
    /// <returns>Returns authentication result with the success for failed state</returns>
    AuthorizationResult Authenticate(string token);
  }
}