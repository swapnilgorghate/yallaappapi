﻿using System.Web.Http;
using FallaApp.Business;
using FallaApp.Business.Impl;
using FallaApp.Data;
using FallaApp.Data.Impl;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;

namespace FallApp.WebApi.App_Start
{
  public static class IOCConfig
  {
    public static void RegisterServices(HttpConfiguration config)
    {
      // =========================================
      // 1. Create a new Simple Injector container
      // =========================================
      var container = new SimpleInjector.Container();

      // UserRegistration
      container.Register<IUserRegistrationManager, UserRegistrationManager>(Lifestyle.Transient);
      container.Register<IUserRegistrationRepository, UserRegistrationRepository>(Lifestyle.Transient);

      // Category
      container.Register<ICategoryManager, CategoryManager>(Lifestyle.Transient);
      container.Register<ICategoryRepository, CategoryRepository>(Lifestyle.Transient);

      //Media
      container.Register<IMediaManager, MediaManager>(Lifestyle.Transient);
      container.Register<IMediaRepository, MediaRepository>(Lifestyle.Transient);

      config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

    }
  }
}
