﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using FallApp.WebApi.App_Start;
using System.Net.Http.Formatting;
using System.Configuration;
using StackExchange.Profiling;

namespace FallaApp.WebApi
{
  public class Global : HttpApplication
  {
    void Application_Start(object sender, EventArgs e)
    {
      // Code that runs on application startup
      AreaRegistration.RegisterAllAreas();
      GlobalConfiguration.Configure(WebApiConfig.Register);
      RouteConfig.RegisterRoutes(RouteTable.Routes);
      GlobalConfiguration.Configure(IOCConfig.RegisterServices);
      //FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);

      GlobalConfiguration.Configuration.Formatters.Clear();
      GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
    }
    protected void Application_BeginRequest(object sender, EventArgs e)
    {
      InitializeProfiler();
      HttpContext.Current.Response.AddHeader("Access-Control-Allow-Origin", "*");

      if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
      {
        // These headers are handling the "pre-flight" OPTIONS call sent by the browser
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, HEAD");
        HttpContext.Current.Response.AddHeader("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");
        HttpContext.Current.Response.AddHeader("Access-Control-Max-Age", "1728000");
        HttpContext.Current.Response.End();
      }

      HttpContext.Current.Response.AddHeader("Access-Control-Expose-Headers", "Date");
    }

    protected void Application_EndRequest()
    {
      MiniProfiler.Stop();
    }

    private void InitializeProfiler()
    {
      bool doProfile = Convert.ToBoolean(ConfigurationManager.AppSettings["Profiling"]);

      if (doProfile)
      {
        MiniProfiler.Start();
      }
    }
  }
}