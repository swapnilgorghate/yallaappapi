﻿using System.Net;
using System.Net.Http;
using FallaApp.Business;
using FallaApp.Model;
using FallaApp.WebApi.Controllers.Base;
using System.Web.Http;
using System.Web;

namespace FallaApp.WebApi.Controllers
{
  public class UserRegistrationController : AuthorizedApiControllerBase
  {
    private IUserRegistrationManager _userRegistrationManager;

    public UserRegistrationController(IUserRegistrationManager userRegistrationManager)
            : base(userRegistrationManager)
    {
      _userRegistrationManager = userRegistrationManager;
    }

    [HttpGet, Route("users")]
    public HttpResponseMessage GetAll()
    {
      var user = HttpContext.Current.Request.UserAgent.ToLower();
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.GetAll());
    }

    [HttpPost, Route("createuser")]
    public HttpResponseMessage Create(User user)
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.Create(user));
    }
    [HttpPut, Route("updateuser")]
    public HttpResponseMessage Update(User user)
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.UpdateUser(user));
    }

    [HttpPost, Route("updateuserpeers")]
    public HttpResponseMessage UpdateUserPeers(User user)
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.UpdateUserPeers(user));
    }

    [HttpGet, Route("countries")]
    public HttpResponseMessage GetAllCountries()
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.GetAllCountries());
    }

    [HttpGet, Route("hobbies")]
    public HttpResponseMessage GetAllHobbies()
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.GetAllHobbies());
    }

    [HttpGet,Route("campuses")]
    public HttpResponseMessage GetAllCampuses()
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.GetAllCampuses());
    }

    [HttpGet, Route("termsandconditions")]
    public HttpResponseMessage TermsAndConditions()
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.TermsAndConditions());
    }

    [HttpPut, Route("validateotp")]
    public HttpResponseMessage ValidateOTP(User user)
    {
      return Request.CreateResponse(HttpStatusCode.OK, _userRegistrationManager.ValidateOTP(user));
    }
  }
}