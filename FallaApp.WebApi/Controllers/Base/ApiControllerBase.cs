﻿using FallaApp.Business;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;

namespace Fallapp.WebApi.Controllers.Base
{
  //[UnhandledExceptionFilterAttribute]
  public class ApiControllerBase : ApiController
  {
    protected readonly IManagerBase BaseManager;

    public ApiControllerBase()
    {
    }

    public ApiControllerBase(IManagerBase baseManager)
    {
      BaseManager = baseManager;
    }

    [NonAction]
    public void SetMangerContext(IRequestContext context)
    {
      var className = string.Empty;
      var userId = string.Empty;
      var tenantId = string.Empty;

      if (Request != null && Request.RequestUri != null)
      {
        className = Request.RequestUri.OriginalString;
      }

      if (context != null)
      {
        userId = context.UserId.ToString();
      }
      

      if (BaseManager != null)
      {
        BaseManager.SetContext(context);
        IEnumerable<FieldInfo> managers = GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance).Where(f => typeof(IManagerBase).IsAssignableFrom(f.FieldType));
        foreach (var manager in managers)
        {
          ((IManagerBase)manager.GetValue(this)).SetContext(context);
        }
      }
    }
  }
}
