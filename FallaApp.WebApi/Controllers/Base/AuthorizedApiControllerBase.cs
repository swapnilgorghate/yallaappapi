﻿using FallaApp.WebApi.Authentication;
using FallaApp.Business;
using FallaApp.WebApi.Controllers.Base;
using Fallapp.WebApi.Controllers.Base;

namespace FallaApp.WebApi.Controllers.Base
{
  /// <summary>
  /// This is a controller that provides authorized access to the action methods.
  /// </summary>
  //[FAAuthorize]
  public class AuthorizedApiControllerBase : ApiControllerBase
  {
    public AuthorizedApiControllerBase()
    {
    }

    public AuthorizedApiControllerBase(IManagerBase baseManager)
        : base(baseManager)
    {
    }
  }
}
