﻿using FallaApp.Business;
using FallaApp.WebApi.Controllers.Base;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using FallaApp.Model;
using System.Web;

namespace FallaApp.WebApi.Controllers
{
  public class AuthenticationController : AuthorizedApiControllerBase
  {

    private IAuthenticationManager _authenticationManager;

    public AuthenticationController(IAuthenticationManager authenticationManager)
            : base(authenticationManager)
    {
      _authenticationManager = authenticationManager;
    }


    [HttpGet, Route("authentication/{authId}")]
    public HttpResponseMessage Get(int authId)
    {
      return Request.CreateResponse(HttpStatusCode.OK, _authenticationManager.Get(authId));
    }
  }
}