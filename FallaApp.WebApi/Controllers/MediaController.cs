﻿using FallaApp.WebApi.Controllers.Base;
using FallaApp.Model;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using FallaApp.Business;
using System.Web;
using Newtonsoft.Json;

namespace FallaApp.WebApi.Controllers
{
  public class MediaController : AuthorizedApiControllerBase
  {
    private readonly IMediaManager _mediaManager;
    private readonly IUserRegistrationManager _userRegistrationManager;

    public MediaController(IMediaManager mediaManager, IUserRegistrationManager userRegistrationManager)
        : base(mediaManager)
    {
      _mediaManager = mediaManager;
      _userRegistrationManager = userRegistrationManager;
    }

    [HttpPost, Route("media/profilepicture")]
    public async Task<HttpResponseMessage> UploadProfileImage()
    {
      try
      {
        if (!Request.Content.IsMimeMultipartContent())
          throw new InvalidOperationException();

        var userjson = HttpContext.Current.Request.Form["userdetails"];
        if (userjson != null)
        {
          User user = JsonConvert.DeserializeObject<User>(userjson);
          _userRegistrationManager.Create(user);
        }        

        var provider = new MultipartMemoryStreamProvider();
        await Request.Content.ReadAsMultipartAsync(provider);

        var file = provider.Contents[0];
        var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
        //filename = filename + DateTime.Now.ToString("ddMMyyyyHHmmssfff");
        var stream = file.ReadAsStreamAsync();

        string profileImageUrl;

        using (var httpStream = await file.ReadAsStreamAsync().ConfigureAwait(false))
        {
          profileImageUrl = _mediaManager.CreateProfileBlob(filename, httpStream);
        }

        return Request.CreateResponse(HttpStatusCode.OK, profileImageUrl);
      }
      catch (Exception ex)
      {
        throw ex.InnerException;
      }
    }
  }
}