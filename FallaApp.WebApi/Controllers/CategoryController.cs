﻿using FallaApp.WebApi.Controllers.Base;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FallaApp.Business;
using FallaApp.Business.Impl;

namespace FallaApp.WebApi.Controllers
{
  public class CategoryController : AuthorizedApiControllerBase
  {
    private readonly ICategoryManager _categoryManager;

    public CategoryController(CategoryManager categoryManager)
    {
      _categoryManager = categoryManager;
    }

    [HttpGet, Route("categories")]
    public HttpResponseMessage GetAll()
    {
      return Request.CreateResponse(HttpStatusCode.OK, _categoryManager.GetAll());
    }
  }
}