﻿CREATE TABLE [dbo].[ClubType] (
    [Id]       INT          IDENTITY (1, 1) NOT NULL,
    [ClubType] NVARCHAR(50) NULL,
    CONSTRAINT [PK_ClubType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

