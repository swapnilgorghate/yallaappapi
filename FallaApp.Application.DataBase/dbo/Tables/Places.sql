﻿CREATE TABLE [dbo].[Places] (
    [Id]                INT              IDENTITY (1, 1) NOT NULL,
    [PlaceName]         NVARCHAR(50)     NULL,
    [Lat]               DECIMAL (18, 12) NULL,
    [Long]              DECIMAL (18, 12) NULL,
    [PlaceType]         INT              NULL,
    [PlaceDescription]  NVARCHAR(1000)   NULL,
    [Address]           NVARCHAR(1000)   NULL,
    [CreatedBy]         BIGINT NOT NULL, 
    [CreatedOn]         DATETIME NOT NULL, 
    [UpdatedBy]         BIGINT NOT NULL, 
    [UpdatedOn]         DATETIME NOT NULL, 
    CONSTRAINT [PK_Places] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Places_PlaceType] FOREIGN KEY ([PlaceType]) REFERENCES [dbo].[PlaceType] ([Id])
);

