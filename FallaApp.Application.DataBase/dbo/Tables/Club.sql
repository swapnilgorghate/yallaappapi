﻿CREATE TABLE [dbo].[Club] (
    [Id]          INT              IDENTITY (1, 1) NOT NULL,
    [ClubName]    NVARCHAR(50)     NULL,
    [Address]     NVARCHAR(1000)   NULL,
    [ClubType]    INT              NULL,
    [Lat]         DECIMAL (18, 12) NULL,
    [Long]        DECIMAL (18, 12) NULL,
    [CampusId]    INT              NULL,
    [CreatedBy]   BIGINT              NOT NULL,
    [CreatedOn]   DATETIME         NOT NULL,
    [UpdatedBy]   BIGINT              NOT NULL,
    [UpdatedOn]   DATETIME         NOT NULL,
    [IsActive]    BIT              NULL,
    CONSTRAINT [PK_Club] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Club_Campus] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[Campus] ([Id]),
    CONSTRAINT [FK_Club_ClubType] FOREIGN KEY ([ClubType]) REFERENCES [dbo].[ClubType] ([Id]),
    CONSTRAINT [FK_Club_User] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Club_User1] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

