﻿CREATE TABLE [dbo].[UserReminders] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [UserId]      BIGINT NULL,
    [EventId]     INT NULL,
    [ReminderId]  INT NULL,
    CONSTRAINT [PK_UserReminders] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_UserReminders_Event] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_UserReminders_Reminder] FOREIGN KEY ([ReminderId]) REFERENCES [dbo].[Reminder] ([Id]),
    CONSTRAINT [FK_UserReminders_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id])
);

