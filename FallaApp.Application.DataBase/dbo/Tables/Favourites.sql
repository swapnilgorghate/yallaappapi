﻿CREATE TABLE [dbo].[Favourites] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [FavouritedBy]  BIGINT NULL,
    [PlaceId]       INT NULL,
    CONSTRAINT [PK_Favourites] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Favourites_Places] FOREIGN KEY ([PlaceId]) REFERENCES [dbo].[Places] ([Id]),
    CONSTRAINT [FK_Favourites_User] FOREIGN KEY ([FavouritedBy]) REFERENCES [dbo].[User] ([Id])
);

