﻿CREATE TABLE [dbo].[Country] (
    [Id]      INT          IDENTITY (1, 1) NOT NULL,
    [Code]    NVARCHAR(10) NULL,
    [Name]    NVARCHAR(50) NULL, 
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([Id] ASC)
);

