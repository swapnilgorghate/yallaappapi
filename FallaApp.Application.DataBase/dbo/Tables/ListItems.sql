﻿CREATE TABLE [dbo].[ListItems] (
    [Id]        INT          IDENTITY (1, 1) NOT NULL,
    [ItemName]  NVARCHAR(50) NULL,
    [ItemType]  INT          NULL,
    [IsPreset]  BIT          NULL,
    [EventId]   INT          NULL,
    [CreatedBy] BIGINT       NOT NULL, 
    [CreatedOn] DATETIME     NOT NULL,
    [UpdatedBy] BIGINT       NOT NULL, 
    [UpdatedOn] DATETIME     NOT NULL, 
    CONSTRAINT [PK_ToDoList] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ListItems_Event] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_ListItems_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_ListItems_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id]),
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'1: To do list 2: What to bring', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'ListItems', @level2type = N'COLUMN', @level2name = N'ItemType';

