﻿CREATE TABLE [dbo].[EventType] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [EventType]   NVARCHAR(50) NULL,
    CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED ([Id] ASC)
);

