﻿CREATE TABLE [dbo].[Describes] (
    [Id]              INT           IDENTITY (1, 1) NOT NULL,
    [DescribeTitle]   NVARCHAR(100) NULL,
    CONSTRAINT [PK_Describes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

