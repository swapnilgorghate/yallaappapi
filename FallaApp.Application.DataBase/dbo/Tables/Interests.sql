﻿CREATE TABLE [dbo].[Interests] (
    [Id]              INT          IDENTITY (1, 1) NOT NULL,
    [InterestTitle]   NVARCHAR(50) NULL,
    CONSTRAINT [PK_Interests] PRIMARY KEY CLUSTERED ([Id] ASC)
);

