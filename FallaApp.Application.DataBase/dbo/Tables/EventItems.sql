﻿CREATE TABLE [dbo].[EventItems] (
    [Id]                INT IDENTITY (1, 1) NOT NULL,
    [ListId]            INT NULL,
    [EventId]           INT NULL,
    [CompletionStatus]  INT NULL,
    [ApprovalStatus]    BIT NULL,
    CONSTRAINT [PK_ListItemStatus] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EventItems_Event] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_EventItems_ListItems] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id])
);