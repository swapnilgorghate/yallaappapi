﻿CREATE TABLE [dbo].[UserType] (
    [Id]        INT          IDENTITY (1, 1) NOT NULL,
    [UserType]  NVARCHAR(50) NULL,
    CONSTRAINT [PK_UserType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

