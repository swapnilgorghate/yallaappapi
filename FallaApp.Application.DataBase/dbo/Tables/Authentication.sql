﻿CREATE TABLE [dbo].[Authentication] (
    [Id]                  INT           IDENTITY (1, 1) NOT NULL,
    [DeviceId]            NVARCHAR(80)  NULL,
    [UserId]              BIGINT           NULL,
    [AuthenticationToken] NVARCHAR(80)  NULL,
    [DeviceToken]         NVARCHAR(100) NULL,
    [DeviceType]          INT           NULL,
    [CreatedBy]           BIGINT NOT NULL, 
    [CreatedOn]           DATETIME DEFAULT (GETDATE()) NOT NULL,
    [UpdatedBy]           BIGINT NOT NULL, 
    [UpdatedOn]           DATETIME  DEFAULT (GETDATE()) NOT NULL,
    [IsActive]            BIT NOT NULL, 
    CONSTRAINT [PK_Authentication] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Authentication_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Authentication_CreateBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Authentication_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

