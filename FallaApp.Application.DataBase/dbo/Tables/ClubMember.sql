﻿CREATE TABLE [dbo].[ClubMember] (
    [Id]            INT IDENTITY (1, 1) NOT NULL,
    [UserId]        BIGINT NULL,
    [ClubId]        INT NULL,
    [IsActive]      BIT NULL,
    [UserLevelId]   INT NULL,
    [CreatedBy]     BIGINT NOT NULL, 
    [CreatedOn]     DATETIME NOT NULL, 
    [UpdatedBy]     BIGINT NOT NULL, 
    [UpdatedOn]     DATETIME NOT NULL, 
    CONSTRAINT [PK_ClubMember] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ClubMember_Club] FOREIGN KEY ([ClubId]) REFERENCES [dbo].[Club] ([Id]),
    CONSTRAINT [FK_ClubMember_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_ClubMember_UserLevel] FOREIGN KEY ([UserLevelId]) REFERENCES [dbo].[UserLevel] ([Id]),
    CONSTRAINT [FK_ClubMember_CreateBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_ClubMember_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

