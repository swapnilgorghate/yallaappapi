﻿CREATE TABLE [dbo].[EventPrivacy] (
    [Id]            INT          IDENTITY (1, 1) NOT NULL,
    [PrivacyDesc]   NVARCHAR(50) NULL,
    CONSTRAINT [PK_EventPrivacy] PRIMARY KEY CLUSTERED ([Id] ASC)
);

