﻿CREATE TABLE [dbo].[PlaceType] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [PlaceType]   NVARCHAR(50) NULL,
    CONSTRAINT [PK_PlaceType] PRIMARY KEY CLUSTERED ([Id] ASC)
);

