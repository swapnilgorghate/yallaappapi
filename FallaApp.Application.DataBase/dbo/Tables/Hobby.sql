﻿CREATE TABLE [dbo].[Hobby] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Name]        NVARCHAR(50) NULL,
    [CreatedBy]   BIGINT NOT NULL, 
    [CreatedOn]   DATETIME NOT NULL, 
    [UpdatedBy]   BIGINT NOT NULL, 
    [UpdatedOn]   DATETIME NOT NULL, 
    CONSTRAINT [PK_Hobby] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Hobby_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Hobby_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

