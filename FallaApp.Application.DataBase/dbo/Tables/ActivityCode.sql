﻿CREATE TABLE [dbo].[ActivityCode] (
    [Id]                  INT          IDENTITY (1, 1) NOT NULL,
    [ActivityCode]        NVARCHAR(50) NULL,
    [ActivityDescription] NVARCHAR(50) NULL,
    [IsEligibleForPoints] BIT          NULL,
    CONSTRAINT [PK_PointCodes] PRIMARY KEY CLUSTERED ([Id] ASC)
);

