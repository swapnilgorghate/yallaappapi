﻿CREATE TABLE [dbo].[Friends] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [FromUserId]  BIGINT NULL,
    [ToUserId]    BIGINT NULL,
    [Status]      INT NULL,
    CONSTRAINT [PK_Friends] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Friends_User] FOREIGN KEY ([ToUserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Friends_User1] FOREIGN KEY ([FromUserId]) REFERENCES [dbo].[User] ([Id])
);

