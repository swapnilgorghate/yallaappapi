﻿CREATE TABLE [dbo].[ActivityLog] (
    [Id]             INT IDENTITY (1, 1) NOT NULL,
    [ActivityCodeId] INT NULL,
    [FromId]         INT NULL,
    [ToId]           INT NULL,
    [FromType]       INT NULL,
    [ToType]         INT NULL,
    CONSTRAINT [PK_ActivityLog] PRIMARY KEY CLUSTERED ([Id] ASC)
);

