﻿CREATE TABLE [dbo].[Chat] (
    [Id]            INT            IDENTITY (1, 1) NOT NULL,
    [EventId]       INT            NULL,
    [SenderId]      BIGINT            NULL,
    [MessageText]   NVARCHAR(1000) NULL,
    [MessageType]   INT            NULL,
    [SentDate]      DATETIME       NULL,
    [IsArchived]    INT            NULL,
    CONSTRAINT [PK_Chat] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Chat_Event] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_Chat_User] FOREIGN KEY ([SenderId]) REFERENCES [dbo].[User] ([Id])
);