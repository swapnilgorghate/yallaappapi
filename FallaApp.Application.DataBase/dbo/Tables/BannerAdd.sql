﻿CREATE TABLE [dbo].[BannerAdd] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [BannerImage] NVARCHAR(50)   NULL,
    [TargetURL]   NVARCHAR(1000) NULL,
    [BannerTitle] NVARCHAR(50)   NULL,
    [IsActive]    BIT            NULL,
    CONSTRAINT [PK_BannerAdd] PRIMARY KEY CLUSTERED ([Id] ASC)
);

