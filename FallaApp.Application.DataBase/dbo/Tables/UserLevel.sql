﻿CREATE TABLE [dbo].[UserLevel] (
    [Id]        INT          IDENTITY (1, 1) NOT NULL,
    [UserLevel] NVARCHAR(50) NULL,
    CONSTRAINT [PK_UserLevel] PRIMARY KEY CLUSTERED ([Id] ASC)
);

