﻿CREATE TABLE [dbo].[Category] (
    [Id]                INT          IDENTITY (1, 1) NOT NULL,
    [CategoryName]      NVARCHAR(50) NULL,
    [EventType]         INT          NULL,
    [IsSpecialCategory] BIT          NULL,
    [IsPreSet]          BIT          NULL,
    [UserId]            BIGINT          NULL,
    [CreatedBy]         BIGINT NOT NULL, 
    [CreatedOn]         DATETIME NOT NULL, 
    [UpdatedBy]         BIGINT NOT NULL, 
    [UpdatedOn]         DATETIME NOT NULL, 
    CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Category_EventType] FOREIGN KEY ([EventType]) REFERENCES [dbo].[EventType] ([Id]),
    CONSTRAINT [FK_Category_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Category_CreateBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Category_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id])
);

