﻿CREATE TABLE [dbo].[EventInvities] (
    [Id]          INT IDENTITY (1, 1) NOT NULL,
    [EventId]     INT NULL,
    [FromUserId]  BIGINT NULL,
    [ToUserId]    BIGINT NULL,
    [Status]      INT NULL,
    CONSTRAINT [PK_EventInvities] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_EventInvities_Event] FOREIGN KEY ([EventId]) REFERENCES [dbo].[Event] ([Id]),
    CONSTRAINT [FK_EventInvities_User] FOREIGN KEY ([FromUserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_EventInvities_User1] FOREIGN KEY ([ToUserId]) REFERENCES [dbo].[User] ([Id])
);

