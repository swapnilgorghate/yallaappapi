﻿CREATE TABLE [dbo].[Notifications] (
    [Id]                INT            NOT NULL,
    [Message]           NVARCHAR(1000) NULL,
    [NotificationType]  INT            NULL,
    [UserId]            BIGINT            NULL,
    [SenderId]          BIGINT            IDENTITY (1, 1) NOT NULL,
    [IsGlobal]          BIT            NULL,
    [ActionId]          INT            NULL,
    CONSTRAINT [PK_Notifications] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Notifications_User] FOREIGN KEY ([UserId]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_Notifications_User1] FOREIGN KEY ([SenderId]) REFERENCES [dbo].[User] ([Id])
);

