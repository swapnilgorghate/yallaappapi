﻿CREATE TABLE [dbo].[Reminder] (
    [Id]           INT             IDENTITY (1, 1) NOT NULL,
    [Reminder]     NVARCHAR(50)    NULL,
    [ReminderTime] DECIMAL (18, 2) NULL,
    CONSTRAINT [PK_Reminder] PRIMARY KEY CLUSTERED ([Id] ASC)
);

