﻿CREATE TABLE [dbo].[Terms] (
    [Id]                  INT  IDENTITY (1, 1) NOT NULL,
    [TermsAndConditions]  TEXT NOT NULL, 
    [CreatedBy]           BIGINT NOT NULL, 
    [CreatedOn]           DATETIME NOT NULL, 
    [UpdatedBy]           BIGINT NOT NULL, 
    [UpdatedOn]           DATETIME NOT NULL,
    CONSTRAINT [FK_TermsAndConditions_CreatedBy] FOREIGN KEY ([CreatedBy]) REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_TermsAndConditions_UpdatedBy] FOREIGN KEY ([UpdatedBy]) REFERENCES [dbo].[User] ([Id]), 
    CONSTRAINT [PK_Terms] PRIMARY KEY ([Id])
);