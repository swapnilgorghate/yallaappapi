CREATE PROCEDURE [dbo].[p_User_Create] 
  @userId BIGINT,
	@MSISDN NVARCHAR(250),
  @deviceId NVARCHAR(50),
  @authenticationToken NVARCHAR(100),
  @deviceToken NVARCHAR(50),
  @deviceType INt

AS
BEGIN
	
  IF  @userId = 0
  BEGIN
  INSERT INTO [dbo].[User] (MSISDN,IsActive) VALUES(@MSISDN,'True')
  SET @userId = @@IDENTITY;
  END
		
  INSERT INTO [dbo].[Authentication] (DeviceId, UserId, AuthenticationToken, DeviceToken ,DeviceType, CreatedBy, CreatedOn, UpdatedBy, UpdatedOn, IsActive) VALUES (@deviceId, @userId, @authenticationToken, @deviceToken, @deviceType,@userId, GETDATE(), @userId,GETDATE(),'True')

  SELECT @userId as Id, @authenticationToken AS AuthenticationToken
END

