﻿using FallaApp.Model;

namespace FallaApp.Data
{
  public interface IAuthenticationRepository
  {
    Authentication Get(int authId);
  }
}
