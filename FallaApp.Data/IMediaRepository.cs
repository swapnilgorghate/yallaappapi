﻿namespace FallaApp.Data
{
  public interface IMediaRepository
  {
    bool UpdateUserProfileImage(long userId, string profilePictureUrl);
  }
}
