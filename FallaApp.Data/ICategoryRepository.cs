﻿using FallaApp.Model;
using System.Collections.Generic;

namespace FallaApp.Data
{
  public interface ICategoryRepository
  {
    IEnumerable<Category> GetAll();
  }
}
