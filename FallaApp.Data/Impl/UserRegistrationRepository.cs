﻿using System.Linq;
using FallaApp.Data.Impl.Base;
using FallaApp.Model;
using Dapper;
using System.Data;
using System.Collections.Generic;
using System;

namespace FallaApp.Data.Impl
{
  public class UserRegistrationRepository : RepositoryBase, IUserRegistrationRepository
  {

    public IEnumerable<User> GetAll()
    {
      return Connection.GetList<User>();
    }
    public User Create(User user)
    {
      User usersData =
        Connection.Query<User>("p_User_Create",
          new
          {
            userId = user.Id,
            MSISDN = user.MSISDN,
            deviceId = user.DeviceId,
            authenticationToken = Guid.NewGuid(),
            deviceToken = user.DeviceToken,
            deviceType = user.DeviceType
          }, commandType: CommandType.StoredProcedure).FirstOrDefault();
      return usersData;
    }
    public int Update(User user)
    {
      return Connection.Update(user);
    }

    public int Update(Authentication authuser)
    {
      return Connection.Update(authuser);
    }
    public Authentication Get(object wherecondition)
    {
      return Connection.GetList<Authentication>(wherecondition).FirstOrDefault();
    }
    public User GetUser(object wherecondition)
    {
      return Connection.GetList<User>(wherecondition).FirstOrDefault();
    }
    public IEnumerable<Country> GetAllCountries()
    {
      return Connection.GetList<Country>();
    }
    public IEnumerable<Hobby> GetAllHobbies()
    {
      return Connection.GetList<Hobby>();
    }
    public IEnumerable<Campus> GetAllCampuses()
    {
      return Connection.GetList<Campus>();
    }
    public IEnumerable<Terms> TermsAndConditions()
    {
      return Connection.GetList<Terms>();
    }
  }
}
