﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using StackExchange.Profiling;

namespace FallaApp.Data.Impl.Base
{
  public abstract class RepositoryBase : IDisposable
  {
    protected IDbConnection Connection;

    protected RepositoryBase()
    {
      SetConnection();
    }

    private void SetConnection()
    {
      var connectionString = ConfigurationManager.ConnectionStrings["FallaApp.ApplicationDatabase"];
      Connection =
        new StackExchange.Profiling.Data.ProfiledDbConnection(new SqlConnection(connectionString.ToString()),
          MiniProfiler.Current);
    }

    protected void OpenConnection()
    {
      if (Connection == null)
      {
        SetConnection();
      }

      if (Connection != null && Connection.State == ConnectionState.Open)
      {
        return;
      }

      Connection.Open();
    }

    protected void CloseConnection()
    {
      if (Connection != null && Connection.State == ConnectionState.Open)
      {
        Connection.Close();
      }
    }

    public void Dispose()
    {
      CloseConnection();
    }
  }
}
