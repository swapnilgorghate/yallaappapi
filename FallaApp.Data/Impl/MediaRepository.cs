﻿using Dapper;
using FallaApp.Data.Impl.Base;


namespace FallaApp.Data.Impl
{
  public class MediaRepository : RepositoryBase, IMediaRepository
  {
    public bool UpdateUserProfileImage(long userId, string profilePictureUrl)
    {
      // update profile picture
      var result =
          Connection.Query("UPDATE UserProfile SET ProfilePicture = @profilePictureURL WHERE userID = @userID ",
              new { profilePictureURL = profilePictureUrl, userID = userId });
      return result != null;
    }
  }
}
