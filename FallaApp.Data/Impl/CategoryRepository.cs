﻿using FallaApp.Data.Impl.Base;
using FallaApp.Model;
using System.Collections.Generic;
using Dapper;

namespace FallaApp.Data.Impl
{
  public class CategoryRepository: RepositoryBase, ICategoryRepository
  {
    public IEnumerable<Category> GetAll()
    {
      return Connection.GetList<Category>();
    }
  }
}
