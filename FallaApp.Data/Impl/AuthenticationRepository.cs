﻿using Dapper;
using FallaApp.Data.Impl.Base;
using FallaApp.Model;

namespace FallaApp.Data.Impl
{
  public class AuthenticationRepository : RepositoryBase, IAuthenticationRepository
  {
    public Authentication Get(int authId)
    {
      return Connection.Get<Authentication>(authId);
    }
  }
}
