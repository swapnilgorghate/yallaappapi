﻿using FallaApp.Model;
using System;
using System.Collections.Generic;


namespace FallaApp.Data
{
  public interface IUserRegistrationRepository
  {
    IEnumerable<User> GetAll();
    User Create(User user);
    int Update(User user);
    int Update(Authentication authuser);
    Authentication Get(object wherecondition);
    User GetUser(object wherecondition);
    IEnumerable<Country> GetAllCountries();
    IEnumerable<Hobby> GetAllHobbies();
    IEnumerable<Campus> GetAllCampuses();
    IEnumerable<Terms> TermsAndConditions();
  }
}
