﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FallaApp.Model
{
  public class User
  {
    public long Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Email { get; set; }
    public Nullable<DateTime> DOB { get; set; }
    public char? Gender { get; set; }
    public long? NationalityId { get; set; }
    public long? CampusId { get; set; }
    public long? HobbyId { get; set; }
    public long? UserTypeId { get; set; }
    public string MSISDN { get; set; }
    public string FBAccount { get; set; }
    public string TWAccount { get; set; }
    public bool LocationVisibility { get; set; }
    public bool ActivityVisibility { get; set; }
    public bool IsActive { get; set; }
    public long? UserLevelId { get; set; }
    public string UserProfileImage { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime UpdatedOn { get; set; }

    //Authenticate details
    [Editable(false)]
    public string DeviceId { get; set; }
    [Editable(false)]
    public string DeviceToken { get; set; }
    [Editable(false)]
    public int? DeviceType { get; set; }
    [Editable(false)]
    public string AuthenticationToken { get; set; }
    [Editable(false)]
    public string OTP { get; set; }
  }
}
