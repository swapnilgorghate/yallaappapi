﻿namespace FallaApp.Model
{
  public class Campus
  {
    public long Id { get; set; }
    public string CampusName { get; set; }
    public string Address { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public decimal Lat { get; set; }
    public decimal Long { get; set; }
  }
}
