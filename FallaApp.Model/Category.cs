﻿
namespace FallaApp.Model
{
  public class Category
  {
    public long Id { get; set; }
    public string CategoryName { get; set; }
    public long EventType { get; set; }
    public bool IsSpecialCategory { get; set; }
    public bool IsPreSet { get; set; }
    public long UserId { get; set; }

  }
}
