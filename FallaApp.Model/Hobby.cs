﻿
namespace FallaApp.Model
{
  public class Hobby
  {
    public long Id { get; set; }
    public string Name { get; set; }
  }
}
