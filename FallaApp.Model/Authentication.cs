﻿using System;

namespace FallaApp.Model
{
  public class Authentication
  {
    public long Id { get; set; }
    public string DeviceId { get; set; }
    public long UserId { get; set; }
    public string AuthenticationToken { get; set; }
    public string DeviceToken { get; set; }
    public int DeviceType { get; set; }
    public DateTime CreatedOn { get; set; }
    public DateTime UpdatedOn { get; set; }
    public bool IsActive { get; set; }
  }
}
